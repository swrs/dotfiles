"a comment
"vim-plug plugin manager
"requires vim-plug to bes installed
call plug#begin('~/.vim/plugged')
Plug 'https://github.com/junegunn/fzf.git', { 'do': { -> fzf#install() } }
Plug 'https://github.com/junegunn/fzf.vim.git'
Plug 'https://github.com/preservim/nerdtree.git'
call plug#end()

"language
lang en_US.UTF-8

"enable syntax processing
syntax enable 

"linebreaks
set wrap
set linebreak
set showbreak=_

"spaces per TAB
set softtabstop=2
set shiftwidth=2
set expandtab

"user interface
set number
set ruler
set showcmd
set wildmenu
set showmatch
set hlsearch
set incsearch
set laststatus=2

"search
nnoremap <Space> :noh<CR>

"windows
set splitright
set splitbelow

"normalize backspace behaviour
set backspace=indent,eol,start

"indentation
filetype indent on
set autoindent

"keys & indings
let mapeader = '\'

"move down/up in visual linebreaks
noremap <Down> gj
noremap <Up> gk

"move between windows
nnoremap <C-S-Up> <C-w><C-K>
nnoremap <C-S-Down> <C-w><C-J>
nnoremap <C-S-Left> <C-w><C-H>
nnoremap <C-S-Right> <C-w><C-L>

"autoclose brackets, parenthesis, etc if they were not closed
inoremap "" ""
inoremap '' ''
inoremap () ()
inoremap [] []
inoremap {} {}
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap (<Space> (<Space><Space>)<left><left>
inoremap [<Space> [<Space><Space>]<left><left>
inoremap {<Space> {<Space><Space>}<left><left>
inoremap (<CR> (<CR>)<ESC>O
inoremap [<CR> [<CR>]<ESC>O
inoremap {<CR> {<CR>}<ESC>O

"emmet config
let g:user_emmet_leader_key = ","

"closetag config
let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.md,*.html.erb"

"fzf fuzzy file search
"set rtp+=/usr/bin/fzf
set rtp+=~/.fzf
let g:fzf_buffers_jump = 1
nnoremap <leader>\ :Files<CR><Esc>
inoremap <leader>\ :BLines<CR><Esc>

"ack grep search
"use ack with ag under the hood
let g:ackprg = "ag --nogroup --nocolor --column"

" nerdtree
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <leader>f :NERDTreeFind<CR>

"searching
set ignorecase
set smartcase

"tags
set tags=tags
